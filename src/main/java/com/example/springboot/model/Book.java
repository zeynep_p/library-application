package com.example.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "books")
public class Book {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "subname")
	private String subname;
	
	@Column(name = "serial_name")
	private String serialName;
	
	@Column(name = "isbn")
	private String isbn;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "authot_name")
	private String authorName;
	
	@Column(name = "publishing_house_name")
	private String publishingHouseName;
	
	public Book() {
		
	}
	

	public Book(long id, String name, String subname, String serialName, String isbn, String description,
			String authorName, String publishingHouseName) {
		super();
		this.id = id;
		this.name = name;
		this.subname = subname;
		this.serialName = serialName;
		this.isbn = isbn;
		this.description = description;
		this.authorName = authorName;
		this.publishingHouseName = publishingHouseName;
	}


	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSubname() {
		return subname;
	}


	public void setSubname(String subname) {
		this.subname = subname;
	}


	public String getSerialName() {
		return serialName;
	}


	public void setSerialName(String serialName) {
		this.serialName = serialName;
	}


	public String getIsbn() {
		return isbn;
	}


	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getAuthorName() {
		return authorName;
	}


	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}


	public String getPublishingHouseName() {
		return publishingHouseName;
	}


	public void setPublishingHouseName(String publishingHouseName) {
		this.publishingHouseName = publishingHouseName;
	}
	
	

}
